<?php

/**
 * I've to apply the $_SESSION superglobal variable for remind the inputed data from the another page
 * First of all session will be start by this syntax
 */

/*
if (!isset($_SESSION)) session_start();
// echo "<pre>";
// var_dump($_POST);
// echo "</pre>";

//bellow this syntax written for not showing the error message from the process.php page
if (isset($_POST['firstName'])) {
    $_SESSION['firstName'] = $_POST['firstName'];
    $_SESSION['lastName'] = $_POST['lastName'];
    $_SESSION['NID'] = $_POST['NID'];
    $_SESSION['bloodGroup'] = $_POST['bloodGroup'];
}


echo "<pre>";
var_dump($_SESSION);
echo "</pre>";

echo "<button type='button><a href='destroy.php'>Destroy Session</a></button>";

* */

if (!isset($_SESSION)) session_start();


if (!empty($_SESSION['PersonInfo'])) $_SESSION['PersonInfo'] .= "[$]";

// if (!isset($_SESSION['PersonInfo'])) $_SESSION['PersonInfo'] = "";

@$_SESSION['PersonInfo'] .= $_POST['firstName'] . "$#$" . $_POST['lastName'] . "$#$" . $_POST['NID'] . "$#$" . $_POST['bloodGroup'] . "[$]";

// echo "<pre>";
// var_dump($_SESSION['PersonInfo']);
// echo "</pre>";

$personWiseInfoArray = explode("[$]", $_SESSION['PersonInfo']);

// echo "<pre>";
// var_dump($personWiseInfoArray);
// echo "</pre>";

// die;
?>

<?php
/**
 * set the cookie into this page;
 */
@$fName = $_POST['firstName'];
@$lName = $_POST['lastName'];
@$nID = $_POST['NID'];
@$bGroup = $_POST['bloodGroup'];

setcookie('firstName', $fName, time() + (86400 * 30), "/"); // 86400 = 1 day
setcookie('lastName', $lName, time() + (86400 * 30), "/"); // 86400 = 1 day
setcookie('NID', $nID, time() + (86400 * 30), "/"); // 86400 = 1 day
setcookie('bloodGroup', $bGroup, time() + (86400 * 30), "/"); // 86400 = 1 day

// var_dump($_COOKIE);
// die();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Table : BOOTSTRAP</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
            <a class="navbar-brand" href="create.php">GO FORM</a>
            <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button> -->
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav" style="list-style-type:none; padding:10px 20px;">
                    <li class="nav-item active btn btn-danger btn-md " style="text-decoration: none; ">
                        <a class="nav-link" style="color:white;" href="destroy.php">DESTROY SESSION</a>
                    </li>
                    <li class="nav-item btn btn-success btn-md" style="text-decoration: none; margin-left: 10px;">
                        <a class="nav-link" style="color:white; list-style-type:none;" href="create.php">ADD NEW PERSON'S INFO</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th colspan="4" class="text-center">
                        <h3>List of Persons Information</h3>
                    </th>
                </tr>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>NID</th>
                    <th>Blood Group</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($personWiseInfoArray as $singlePersonInfoString) {

                    $singlePersonInfoArray = explode("$#$", $singlePersonInfoString);



                    echo "<tr>";
                    echo " <td>$singlePersonInfoArray[0]</td>";
                    if (isset($singlePersonInfoArray[1])) echo " <td>$singlePersonInfoArray[1]</td>";
                    if (isset($singlePersonInfoArray[2])) echo " <td>$singlePersonInfoArray[2]</td>";
                    if (isset($singlePersonInfoArray[3])) echo " <td>$singlePersonInfoArray[3]</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
    </div>

</body>

</html>