<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BOOTSTRAP: ADVANCED FORM</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body>
    <form action="process.php" method="post">
        <div class="container" style="padding:40px 0px; background: green; color:white; font-size: 20px; font-weight:bold;">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 style="font-size: 49px; padding: 30px 0px; text-shadow: 5px 5px 10px red, 0.2em 0.2em 0.5em blue, 1em 1em 2em blue;">
                    Personal Information Form
                </h2>
            </div>
            <div class="col-lg-8 col-lg-offset-2">
                <div class="form-group" style="padding: 10px 0px;">
                    <label for="firstName">First Name</label>
                    <input style="padding: 25px 20px; border-radius:20px; box-shadow: 5px 5px 10px 2px rgba(0,0,0,.8);" class="form-control" type="text" name="firstName" id="firstName" placeholder="Enter First name" required>
                </div>
                <div class=" form-group" style="padding: 10px 0px;">
                    <label for="lastName">Last Name</label>
                    <input style="padding: 25px 20px; border-radius:20px; box-shadow: 5px 5px 10px 2px rgba(0,0,0,.8);" class="form-control" type="text" name="lastName" id="lastName" placeholder=" Enter last name" required>
                </div>
                <div class=" form-group" style="padding: 10px 0px;">
                    <label for="NID">NID</label>
                    <input style="padding: 25px 20px; border-radius:20px; box-shadow: 5px 5px 10px 2px rgba(0,0,0,.8);" class="form-control" type="number" name="NID" id="NID" placeholder="Enter identity number">
                </div>
                <div class="" style=" padding: 10px 0px;">
                    <label for="bloodGroup">Blood Group : </label>
                    <select style="font-size: 16px; padding: 14px 0px; color:black; border:none; border-radius:20px; box-shadow: 5px 5px 10px 2px rgba(0,0,0,.8); text-align:center;" name="bloodGroup" id="bloodGroup" required>
                        <option value=" select">Select your BG</option>
                        <option value="A+">A+</option>
                        <option value="A-">A-</option>
                        <option value="AB+">AB+</option>
                        <option value="AB-">AB-</option>
                        <option value="B+">B+</option>
                        <option value="B-">B-</option>
                        <option value="O+">O+</option>
                        <option value="O-">O-</option>
                    </select>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="rememberMe"> Remember me
                    </label>
                </div>
                <div class="form-group" style="padding: 10px 0px;">
                    <input class="btn btn-default btn-lg" style="padding:15px 40px; color:black; font-size: 24px; font-weight:bold; border-radius:20px; box-shadow: 5px 5px 10px 2px rgba(0,0,0,.8);" type="submit" value="Submit">
                </div>
            </div>

        </div>

    </form>
</body>

</html>